import cv2
import numpy as np
import matplotlib.pyplot as plt
import pytesseract
import imutils
import os
import re
import sys

# images = convert_from_path("D:\\#Python\\TransOil\\Text_recog\\examples\\test.pdf",
#                             500,
#                             poppler_path = r"C:\path\to\poppler-xx\bin"
#                             )
# print(images)
# for i, image in enumerate(images):
#     fname = 'image'+str(i)+'.png'
#     image.save(fname, "PNG")

img_show = 1
column_num = 3


# Rotation of the image
def img_rotate(image, angle):
    rotated = imutils.rotate_bound(image, angle)
    return rotated

# Determine the orientation of the cells that recognize


def sort_contours(cnts, method="left-to-right"):
    # initialize the reverse flag and sort index
    reverse = False
    i = 0
    # handle if we need to sort in reverse
    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True
    # handle if we are sorting against the y-coordinate rather than
    # the x-coordinate of the bounding box
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1
    # construct the list of bounding boxes and sort them from top to
    # bottom
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
                                        key=lambda b: b[1][i], reverse=reverse))
    # return the list of sorted contours and bounding boxes
    return (cnts, boundingBoxes)


# Recognizing the text for the table rotation
def side_orientation(text):
    for i in text:
        if i in ["Деталь", "деталь", "детали", "Причина", "причина", "замены",
                 "согласование", "Основание", "основание"
                 ]:
            return True
    return False


# Creation of the counturs
def counturs_creation(img):
    # Contours creation
    # thresholding the image to a binary image
    thresh, img_bin = cv2.threshold(
        img, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    if img_show == 1:
        print("Counturs creation function - input img")
        plotting = plt.imshow(img_bin)
        plt.show()

    # inverting the image
    img_bin = 255-img_bin
    # plotting = plt.imshow(img_bin)
    # plt.show()

    # countcol(width) of lines as 100th of total width
    lines_len = np.array(img).shape[1]//100

    ver_lines = cv2.getStructuringElement(cv2.MORPH_RECT, (1, lines_len))
    hor_lines = cv2.getStructuringElement(cv2.MORPH_RECT, (lines_len, 1))
    lines = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))

    # Use vertical lines to detect and save the vertical lines in a jpg
    image_1 = cv2.erode(img_bin, ver_lines, iterations=3)
    vertical_lines = cv2.dilate(image_1, ver_lines, iterations=3)

    if img_show == 1:
        print("Counturs creation function - vertical_lines")
        plotting = plt.imshow(vertical_lines)
        plt.show()

    # Use horizontal lines to detect and save the horizontal lines in a jpg
    image_2 = cv2.erode(img_bin, hor_lines, iterations=3)
    horizontal_lines = cv2.dilate(image_2, hor_lines, iterations=3)
    if img_show == 1:
        print("Counturs creation function - horizontal_lines")
        plotting = plt.imshow(horizontal_lines)
        plt.show()

    # Combine horizontal and vertical lines in a new third image, with both having same weight.
    img_vh = cv2.addWeighted(vertical_lines, 0.5, horizontal_lines, 0.5, 0.0)
    if img_show == 1:
        print("Counturs creation function - img_vh")
        plotting = plt.imshow(img_vh)
        plt.show()

    # Eroding and thesholding the image
    img_vh = cv2.erode(~img_vh, lines, iterations=2)
    if img_show in [1, 2]:
        print("Counturs creation function - img_vh - eroded")
        plotting = plt.imshow(img_vh)
        plt.show()

    thresh, img_vh = cv2.threshold(
        img_vh, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    # Detect contours for following box detection
    contours, hierarchy = cv2.findContours(
        img_vh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # Sort all the contours by top to bottom.
    reverse = False
    boundingBoxes = [cv2.boundingRect(c) for c in contours]
    (cnts, boundingBoxes) = zip(*sorted(zip(contours, boundingBoxes),
                                        key=lambda b: b[1][1], reverse=reverse))

    if img_show == 2:
        for i, c in enumerate(contours):
            tmp_img = np.zeros(img.shape, dtype=np.uint8)
            res = cv2.drawContours(tmp_img, [c], -1, 255, cv2.FILLED)
            print("Plotting res")
            plotting = plt.imshow(res)
            plt.show()

        # Create list box to store all boxes in
    box = []
    # Get position (x,y), width and height for every contour and show the contour on image
    img_rectang = None
    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        if (w < int(w_img/3) and w > int(w_img/200) and h < int(h_img/4) and h > int(h_img/100)):
            img_rectang = cv2.rectangle(
                img_vh, (x, y), (x+w, y+h), (0, 255, 255), 10)
            box.append([x, y, w, h])

    if img_show in [1, 2]:
        print("Counturs creation function - img_rectang")
        plotting = plt.imshow(img_rectang)
        plt.show()
    img_rectang = None
    return img_bin, box, contours


# Recognizing the cells for the table rotation
def side_definition(img):
    # Determining the orientation of the table
    img_bin, box, contours = counturs_creation(img)
    temp_list = []
    for i in box:
        temp_list.append(i[2])
        temp_list.sort()
    min_w = min(temp_list)
    text_cw = None
    text_ccw = None
    rotation_status = None
    temp_list.clear()

    for i in box:
        if i[2] < min_w*1.1:
            temp_list.append(i)

    for cell in temp_list:
        img_cut = img_bin[cell[1]:(
            cell[1]+cell[3]), cell[0]:(cell[0]+cell[2])]
        img_cut_cw = img_rotate(img_cut, 90)
        img_cut_ccw = img_rotate(img_cut, -90)
        if not rotation_status:
            text_cw = pytesseract.image_to_string(
                img_cut_cw, lang="rus", config='--psm 6')
            text_ccw = pytesseract.image_to_string(
                img_cut_ccw, lang="rus", config='--psm 6')
            text_cw = side_orientation(text_cw.split())
            text_ccw = side_orientation(text_ccw.split())
            if not text_cw and not text_ccw:
                rotation_status = None
            else:
                rotation_status = True
        # plotting = plt.imshow(img_cut, cmap='gray')
        # plt.show()

    if text_cw:
        rotation_side = "CW"
        img = img_rotate(img, 90)
        orientation = "H"

    if text_ccw:
        rotation_side = "CCW"
        img = img_rotate(img, -90)
        orientation = "H"

    if not text_cw and not text_ccw:
        print("Orientation ERROR")

    return orientation, img


def check_cell(dict):
    columns = len(dict.keys())

    lines = None
    for k, v in dict.items():
        if not lines:
            lines = len(v)
        if lines <= len(v):
            lines = len(v)
    # print(columns, "x", lines)

    # print("Run function cells_checker")
    list_col_x = []
    list_col_y = []
    list_cell_h = []
    list_cell_w = []
    for k, v in columns_dict.items():
        for i in v:
            list_col_x_tmp = [i[0] for i in v]
            list_col_y_tmp = [i[1] for i in v]
            list_cell_w_tmp = [i[2] for i in v]
            list_cell_h_tmp = [i[3] for i in v]
        list_col_y.append(list_col_y_tmp)
        list_col_x.append(list_col_x_tmp)
        list_cell_h.append(list_cell_w_tmp)
        list_cell_w.append(list_cell_h_tmp)

    for i in list_col_x:
        if len(i) < lines:
            i.append(int(sum(i)/len(i)))

    for i in list_col_y:
        if len(i) == lines:
            ref_list_col_y = i

    for i in list_col_y:
        if len(i) < lines:
            for v in range(lines):
                ref_val = ref_list_col_y[v]
                for i in list_col_y:
                    f_num = False
                    temp = []
                    for k in range(len(i)):
                        if i[k] in range(
                            int(ref_val*0.95),
                            int(ref_val*1.05)
                              ) and f_num is False:
                            f_num = True
                            temp.append(f_num)
                        else:
                            f_num = False
                            temp.append(f_num)
                    if True not in temp:
                        i.append(ref_val)
                        i.sort()

    for i in list_cell_h:
        if len(i) < lines:
            i.append(int(sum(i)/len(i)))

    for i in list_cell_w:
        if len(i) == lines:
            ref_list_cell_w = i

    for i in list_cell_w:
        if len(i) < lines:
            for v in range(lines):
                ref_val = ref_list_cell_w[v]
                for i in list_cell_w:
                    f_num = False
                    temp = []
                    for c in range(len(i)):
                        if i[c] in range(
                            int(ref_val*0.95),
                            int(ref_val*1.05)
                              ) and f_num is False:
                            f_num = True
                            temp.append(f_num)
                        else:
                            f_num = False
                            temp.append(f_num)
                    if True not in temp:
                        i.append(ref_val)
                        i.sort()
                    for c in range(len(ref_list_cell_w)):
                        try:

                            if i[c] not in range(int(ref_list_cell_w[c]*0.95), int(ref_list_cell_w[c]*1.05)):
                                i.append(ref_list_cell_w[c])
                                i.sort()
                        except:
                            i.append(ref_list_cell_w[c])

    counter = 0
    for k, v in dict.items():
        a = list(zip(list_col_x[counter], list_col_y[counter],
                 list_cell_h[counter], list_cell_w[counter]))
        counter += 1
        dict[k] = a
    return dict


pytesseract.pytesseract.tesseract_cmd = "C:\\Program Files\\Tesseract-OCR\\tesseract.exe"

# images = "D:\\#Python\\TransOil\\Text_recog\\examples\\text1.jpg"
folder = "D:\\#Python\\TransOil\\Text_recog\\examples"
for subdir, dirs, files in os.walk(folder):
    for item in os.listdir(folder):
        # Input image
        images = folder + "\\" + item
        print(images)

        # Step1
        # Read the image in gray
        img = cv2.imread(images)
        imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_copy = imgray.copy()
        h_img, w_img = img.shape[:2]

        if img_show in [1, 2]:
            print("Main - circle+text")
            plotting = plt.imshow(img_copy, cmap='gray')
            plt.show()

        # Check orientation on the file
        orientation = None
        if h_img > w_img:
            orientation = "V"
            orientation, img = side_definition(img_copy)
            img_copy = img.copy()
        else:
            orientation = "H"

        # Search and create the contours of the table
        img_bin, box, contours = counturs_creation(imgray)

        # In contours create the the "boxes" of the cells
        for i in box:
            image2 = cv2.circle(img, (i[0], i[1]), 50, (255, 0, 0), 10)
            text_new = (f'{i[0]}, {i[1]}')
            image2 = cv2.putText(
                image2, text_new, (i[0], i[1]), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255))
        if img_show in [1, 2]:
            print("Main - circle+text")
            plotting = plt.imshow(image2, cmap='gray')
            plt.show()
        image2 = None

        box = sorted(box, key=lambda x: x[0])

        # Columns detection
        columns_dict = {}
        counter = 1
        for i in range(len(box)):
            if i == 0:
                previous = box[i][0]
                columns_dict[counter] = [box[i]]
            else:
                if previous*0.95 < box[i][0] < previous*1.05:
                    temp = columns_dict[counter]
                    temp.append(box[i])
                    columns_dict[counter] = temp
                    previous = box[i][0]
                else:
                    counter += 1
                    columns_dict[counter] = [box[i]]
                    previous = box[i][0]

        # Sort the required columns from the table
        for i in columns_dict[column_num]:
            image3 = cv2.rectangle(
                img_copy, (i[0], i[1]), (i[0]+i[2], i[1]+i[3]), (0, 0, 255), 10)
        if img_show in [1, 2]:
            print("Main - rectangle of the column")
            plotting = plt.imshow(image3, cmap='gray')
            plt.show()

        # Text reqognizing from the selected cells
        final_text = []
        print_text = []

        # Sort the columns from top to bottom
        for k, v in columns_dict.items():
            columns_dict[k] = sorted(
                    columns_dict[k], key=lambda x: x[1])

        top_line_table = []
        first_line_size = []
        # Идентификация граней таблицы для поиска текста над ней.
        for key in columns_dict:
            for x in columns_dict[key]:
                top_line_table.append(x[1])
                first_line_size.append(x[3])
        # Vagon number.
        img_cut = img_bin[
            (min(top_line_table) - min(first_line_size)*5): min(top_line_table),
            0:
             ]
        text = pytesseract.image_to_string(
            img_cut, lang="rus", config='--psm 6')
        text = ''.join(text.split())
        vagon_id = re.search(r"\d\d\d\d\d\d\d\d", text)
        vagon_id = vagon_id[0]
        print(vagon_id)
        #
        # for i in columns_dict.items():
        #     print(len(i[1]))
        temp_list = []
        for i in columns_dict.items():
            temp_list.append(len(i[1]))

        if max(temp_list) != (sum(temp_list)/len(temp_list)):
            columns_dict = check_cell(columns_dict)
            for i in columns_dict[column_num]:
                image3 = cv2.rectangle(
                    img_copy, (i[0], i[1]), (i[0]+i[2], i[1]+i[3]), (0, 0, 255), 10)
            if img_show in [1, 2]:
                print("Main - rectangle of the column")
                plotting = plt.imshow(image3, cmap='gray')
                plt.show()

        # Data collection form the required columns
        for cell in columns_dict[column_num]:
            height = cell[3]
            wide = cell[2]
            img_cut = img_bin[cell[1]:(
                cell[1]+cell[3]), cell[0]:(cell[0]+cell[2])]
            # img_cut = cv2.resize(img_cut,(0,0),fx=3,fy=3)
            # img_cut = cv2.GaussianBlur(img_cut,(11,11),0)
            # img_cut = cv2.medianBlur(img_cut,5)
            text = pytesseract.image_to_string(
                img_cut, lang="rus", config='--psm 6')
            text = ''.join(text.split())
            text = text.replace('.0.', ".о.")
            match = re.search(
                r"о.-\d\d*мм|о.-\d\d.мм|о.-\d\d.\dмм", text, re.IGNORECASE)
            # print(match[0][2:] if match else None)
            final_text.append(text)
            if match:
                print_text.append(match[0][2:])

        final_text = []
        for i, c in enumerate(columns_dict):
            cells = columns_dict[c]
            temp_list = []
            for cell in cells:
                height = cell[3]
                wide = cell[2]
                img_cut = img_bin[cell[1]:(
                    cell[1]+cell[3]), cell[0]:(cell[0]+cell[2])]
                # img_cut = cv2.resize(img_cut,(0,0),fx=3,fy=3)
                # img_cut = cv2.GaussianBlur(img_cut,(11,11),0)
                # img_cut = cv2.medianBlur(img_cut,5)
                text = pytesseract.image_to_string(
                    img_cut, lang="rus", config='--psm 6')
                # text = text.replace("\n", " ")
                text = text.replace("\x0c", "")
                temp_list.append(text)
            final_text.append(temp_list)

        image3 = None
        print(print_text)
        # print(final_text)

        with open(f'Vagon {vagon_id}.txt', "w") as f:
            for i in final_text:
                f.write("\n")
                f.write("".join(i))
