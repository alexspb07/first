
from functools import wraps
from collections import namedtuple


def return_namedtuple(*param):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            if isinstance(result, tuple):
                    Action = namedtuple('Action', param)
                    name_tuple = Action._make(func())
                    return name_tuple
            return result
        return wrapper
    return decorator


@return_namedtuple('one', 'two')
def f1():
    return 1, 2


@return_namedtuple('a', 'b', 'c')
def f2():
    return 'Python', 'is', 'programming language'

@return_namedtuple()
def f():
    return 123

r = f1()
print(r.one)
print(r.two)

r = f2()
print(r.c)

r = f()
print(r)
